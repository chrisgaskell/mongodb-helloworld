﻿using System.Collections.Generic;
using MongoDB.Driver;
using MongoHW.Domain.Models;

namespace MongoHW.Domain.Data
{
    public class CommentsRepository : ICommentsRepository
    {
        private readonly string _connectionString;
        private MongoCollection<Comment> _commentCollection;

        public CommentsRepository()
        {
            _connectionString = "mongodb://mongo-test:27017/MongoHW";
            _commentCollection = null;
        }

        private MongoCollection<Comment> CommentCollection()
        {
            if (_commentCollection != null) return _commentCollection;

            var mongoUrl = new MongoUrl(_connectionString);
            var mongoClient = new MongoClient(mongoUrl);
            var mongoServer = mongoClient.GetServer();
            var mongoDatabase = mongoServer.GetDatabase(mongoUrl.DatabaseName);

            _commentCollection = mongoDatabase.GetCollection<Comment>("comments");

            return _commentCollection;
        }

        public Comment Save(Comment comment)
        {
            CommentCollection().Save(comment);
            return comment;
        }

        public IEnumerable<Comment> GetAll()
        {
            return CommentCollection().FindAll();
        }
    }
}