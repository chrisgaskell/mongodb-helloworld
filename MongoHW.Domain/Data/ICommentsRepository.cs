﻿using System.Collections.Generic;
using MongoHW.Domain.Models;

namespace MongoHW.Domain.Data
{
    public interface ICommentsRepository
    {
        Comment Save(Comment comment);
        IEnumerable<Comment> GetAll();
    }
}