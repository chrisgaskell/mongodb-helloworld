﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;

namespace MongoHW.Domain.Models
{
    public class Comment
    {
        public Comment()
        {
            DateCreated = DateTime.Now;
        }

        public ObjectId Id { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DisplayName("Comment")]
        public string CommentText { get; set; }

        [DisplayName("Date Created")]
        public DateTime DateCreated { get; set; }
    }
}