﻿using System.Web.Mvc;
using MongoHW.Domain.Data;
using MongoHW.Domain.Models;
using MongoHW.Web.Models;

namespace MongoHW.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly CommentsRepository _commentsRepository;

        public HomeController()
        {
            _commentsRepository = new CommentsRepository();
        }

        //
        // GET: /Home/
        public ActionResult Index()
        {
            var model = new HomeViewModel();
            model.Comments = _commentsRepository.GetAll();
            return View(model);
        }

        //
        // POST: /Index/Create
        [HttpPost]
        public ActionResult Index(Comment comment)
        {
            if (ModelState.IsValid)
            {
                _commentsRepository.Save(comment);
                return RedirectToAction("Index");
            }

            var model = new HomeViewModel {NewComment = comment};
            model.Comments = _commentsRepository.GetAll();
            return View(model);
        }

        //
        // GET: /Home/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Home/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Home/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Home/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}