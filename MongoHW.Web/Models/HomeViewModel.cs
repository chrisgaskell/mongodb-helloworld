﻿using System.Collections.Generic;
using MongoHW.Domain.Models;

namespace MongoHW.Web.Models
{
    public class HomeViewModel
    {
        public IEnumerable<Comment> Comments { get; set; }
        public Comment NewComment { get; set; }
    }
}